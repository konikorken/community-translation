import { LightningElement, api } from 'lwc';

export default class TranslatableComponentLWC extends LightningElement {
    @api translatedText;

}