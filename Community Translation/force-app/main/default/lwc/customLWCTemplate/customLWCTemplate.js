import { LightningElement, api } from 'lwc';

export default class CustomLWCTemplate extends LightningElement {
    @api translatedText;
}